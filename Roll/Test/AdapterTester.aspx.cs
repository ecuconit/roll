﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Roll
{
	public partial class AdapterTester : System.Web.UI.Page
	{
		#region Page Constructor
		/// <summary>
		/// On load, run tests
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Page_Load(object sender, EventArgs e)
		{
		}
		#endregion

		#region Helper Method
		/// <summary>
		/// Write a test message to display, allow for error flagging
		/// </summary>
		/// <param name="message">What message to write</param>
		/// <param name="isError">Whether or not this is an error message</param>
		private void ToDebug(string message, bool? isError = null)
		{
			Response.Write(string.Format("<span style=\"color: {2}\">[{0}] - {1}</span>\r\n<br />",
				DateTime.Now.ToString("yyyy/MM/d hh:mm:ss.F"),
				message,
				isError == null ? "#000000" : (bool)isError ? "#721c24" : "#155724"));
		}
		#endregion
	}
}