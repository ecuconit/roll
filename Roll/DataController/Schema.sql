USE [Dice]
GO
/****** Object:  Table [dbo].[Roll]    Script Date: 4/29/2020 9:28:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roll](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Enabled] [bit] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Name] [varchar](150) NOT NULL,
	[IP] [varchar](50) NOT NULL,
	[Input] [varchar](500) NOT NULL,
	[Values] [varchar](max) NOT NULL,
	[Total] [int] NOT NULL,
 CONSTRAINT [PK_Roll] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Roll] ADD  CONSTRAINT [DF_Roll_Enabled]  DEFAULT ((1)) FOR [Enabled]
GO
ALTER TABLE [dbo].[Roll] ADD  CONSTRAINT [DF_Roll_IP]  DEFAULT ('127.0.0.1') FOR [IP]
GO
/****** Object:  StoredProcedure [dbo].[DeleteRolls]    Script Date: 4/29/2020 9:28:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteRolls]

AS

BEGIN

	UPDATE [Roll]
	SET [Enabled] = 0
	FROM [Roll]

	RETURN 1

END
GO
/****** Object:  StoredProcedure [dbo].[InsertRoll]    Script Date: 4/29/2020 9:28:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[InsertRoll]
(
	@name VARCHAR(150),
	@ip VARCHAR(50),
	@input VARCHAR(500),
	@values VARCHAR(MAX),
	@total INT
)

AS

BEGIN

	/* Insert into database */
	INSERT INTO [dbo].[Roll]
		([Enabled], [Date], [Name], [IP], [Input], [Values], [Total])
	VALUES
		(1, GETDATE(), @name, @ip, @input, @values, @total)

	/* Verification */
	IF (@@ERROR <> 0)
		RETURN -1
	ELSE
		RETURN @@IDENTITY

END
GO
/****** Object:  StoredProcedure [dbo].[SelectRolls]    Script Date: 4/29/2020 9:28:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SelectRolls]
(
	@pageSize INT,
	@lastMessage INT
)

AS 

BEGIN

	/* Check for paging type */
	SELECT TOP (@pageSize) *
	FROM [Roll]
	WHERE [Enabled] = 1 AND [ID] > @lastMessage
	ORDER BY [Date] DESC

END
GO
