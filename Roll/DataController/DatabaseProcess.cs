﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text.RegularExpressions; // Regular expression
using System.Configuration;

namespace Roll
{
	public class DatabaseProcess
	{
		#region User Region
		/// <summary>
		/// Save a roll object to the database
		/// </summary>
		/// <param name="Name">User's name</param>
		/// <param name="IP">Client IP address</param>
		/// <param name="Input">User input</param>
		/// <param name="Values">Resulting values</param>
		/// <param name="Total">Result total</param>
		/// <param name="errorMessage">Error message, if any, null otherwise</param>
		/// <returns>New roll object's ID</returns>
		public static int InsertRoll(string Name, string IP, string Input, string Values, int Total, out string errorMessage)
		{
			// Initialize
			errorMessage = null;

			// Construct parameters
			Dictionary<string, object> param = new Dictionary<string, object>();
			param.Add("@name", Name);
			param.Add("@ip", IP);
			param.Add("@input", Input);
			param.Add("@values", Values);
			param.Add("@total", Total);

			// Make controller call
			object reply = DatabaseController.InsertUpdateReturn("InsertRoll", param, ref errorMessage);

			// Check for result
			if ((reply == null) || (reply.GetType() != typeof(int)) || ((int)reply <= 0))
			{
				errorMessage = "Unable to successfully insert data using [Insert Roll] stored procedure call.";
				return -1;
			}
			return (int)reply;
		}

		/// <summary>
		/// Select all rolls from database
		/// </summary>
		/// <param name="PageSize">How many to get</param>
		/// <param name="LastMessage">When was the last message obtained, -1 for none</param>
		/// <param name="errorMessage">Error message, if any, null otherwise</param>
		/// <returns>A database entry representing the roll, null if error</returns>
		public static DataTable SelectRolls(int PageSize, int LastMessage, out string errorMessage)
		{
			// Initialize
			errorMessage = null;

			// Construct parameters
			Dictionary<string, object> param = new Dictionary<string, object>();
			param.Add("@pageSize", PageSize);
			param.Add("@lastMessage", LastMessage);

			// Make controller call
			DataTable reply = DatabaseController.SelectReturn("SelectRolls", param, ref errorMessage);

			// Check for result
			if (reply == null)
			{
				errorMessage = "Failed to get data from [Select Rolls] stored procedure call.";
				return null;
			}
			return reply;
		}

		/// <summary>
		/// Wipe active logs from the database
		/// </summary>
		/// <param name="errorMessage">Error message, if any, null otherwise</param>
		/// <returns>Confirmation number</returns>
		public static int WipeRolls (out string errorMessage)
		{
			// Initialize
			errorMessage = null;

			// Make controller call
			object reply = DatabaseController.InsertUpdateReturn("DeleteRolls", new Dictionary<string, object>(), ref errorMessage);

			// Check for result
			if ((reply == null) || (reply.GetType() != typeof(int)) || ((int)reply <= 0))
			{
				errorMessage = "Unable to successfully wipe data using [Delete Rolls] stored procedure call.";
				return -1;
			}
			return (int)reply;
		}
		#endregion
	}
}