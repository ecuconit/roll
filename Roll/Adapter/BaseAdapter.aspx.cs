﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;

namespace Roll.Adapter
{
	public partial class BaseAdapter : System.Web.UI.Page
	{
		#region Private Data Holder
		public static string OkayMessage = "Okay";
		protected static JavaScriptSerializer serializer = new JavaScriptSerializer();
		#endregion

		#region Helper Method
		/// <summary>
		/// Convert a nullable bool object to a string
		/// </summary>
		/// <param name="input">Original input</param>
		/// <param name="defaultText"></param>
		/// <returns>Yes, if true; No, if false; default text if null</returns>
		protected static string BoolToString(bool? input, string defaultText = "")
		{
			return (input == null ? defaultText : ((bool)input ? "Yes" : "No"));
		}
		#endregion
	}
}