﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using Newtonsoft.Json;
using System.Data;

namespace Roll.Adapter
{
	public partial class RollAdapter : BaseAdapter
	{
		/// <summary>
		/// Load logged rolls from the server
		/// </summary>
		/// <param name="PageSize">How many to get</param>
		/// <param name="LastMessage">When was the last message obtained, -1 for none</param>
		/// <returns>Serialized roll log objects</returns>
		[WebMethod]
		public static string GetLogs(int PageSize, int LastMessage)
		{
			// Get values
			string errorMessage = null;
			DataTable reply = DatabaseProcess.SelectRolls(PageSize, LastMessage, out errorMessage);

			// Verify reply
			if ((reply == null) || (errorMessage != null))
				return "Unable to fetch data successfully.  Error Message: " + (errorMessage ?? "Unknown Error");
			return JsonConvert.SerializeObject(reply.AsEnumerable().Reverse().Select(x => new
			{
				ID = x["ID"],
				Date = x["Date"].ToString(),
				Name = x["Name"],
				IP = x["IP"],
				Input = x["Input"],
				Values = x["Values"],
				Total = x["Total"]
			}));
		}

		/// <summary>
		/// Save a roll log to the server, rolled on server side
		/// </summary>
		/// <param name="Name">User's name</param>
		/// <param name="Input">User input</param>
		/// <param name="Dice">A list of (die count, face) pairs</param>
		/// <returns>New log object ID</returns>
		[WebMethod]
		public static string SaveLog(string Name, string Input)
		{
			// Simple verification
			string errorMessage = null;
			List<Tuple<int, int, int>> dice = Parse(Input, out errorMessage);
			if ((dice == null) || (dice.Count <= 0) || (errorMessage != null))
				return "Unable to parse command successfully.  Error Message: " + (errorMessage ?? "Unknown Error");

			// Random roll and save to database
			List<int> values = Roll(dice);
			int id = DatabaseProcess.InsertRoll(Name, GetIP(), Input, string.Join(", ", values), values.Sum(), out errorMessage);

			// Verify reply
			if ((id <= 0) || (errorMessage != null))
				return "Unable to insert data successfully.  Error Message: " + (errorMessage ?? "Unknown Error");
			return id.ToString();
		}

		/// <summary>
		/// On call, disable all logs on the server
		/// </summary>
		/// <returns>Okay message</returns>
		[WebMethod]
		public static string ClearLogs()
		{
			// Simple verification
			string errorMessage = null;
			int reply = DatabaseProcess.WipeRolls(out errorMessage);

			// Verify reply
			if ((reply <= 0) || (errorMessage != null))
				return "Unable to insert data successfully.  Error Message: " + (errorMessage ?? "Unknown Error");
			return OkayMessage;
		}

		#region Helper Methods
		/// <summary>
		/// Parse an input into a list of dice pairs in (count, side) order
		/// </summary>
		/// <param name="input">Input text</param>
		/// <param name="errorMessage">Error message if any, null otherwise</param>
		/// <returns>A list of dice roll pairs</returns>
		private static List<Tuple<int, int, int>> Parse(string input, out string errorMessage)
		{
			// Simple verification
			errorMessage = null;
			if (string.IsNullOrEmpty(input))
			{
				errorMessage = "Empty input string";
				return null;
			}

			// Parse commands
			List<Tuple<int, int, int>> result = new List<Tuple<int, int, int>>();
			List<int> split;
			List<string> errors = new List<string>();
			int temp;
			result = input.Split(' ').ToList().Select(x =>
			{
				split = x.Split(new char[] { 'd', 'D' }).Select(y => int.TryParse(y, out temp) ? temp : -1).ToList().ToList();
				if ((split.Count <= 0) || (split.All(y => y == -1)))
				{
					errors.Add(x);
					return null;
				}

				// COnstruct
				if (split.Count == 1)
					return new Tuple<int, int, int>(1, split[0], -1);
				if (split[0] == -1)
					split[0] = 1;
				split = split.Select(y => y < 0 ? 6 : y).ToList();
				return new Tuple<int, int, int>(split[0], split[1], split.Count > 2 ? split[2] : -1);
			}).ToList();

			// Final verification
			if (errors.Count > 0)
			{
				errorMessage = "Unable to parse following input(s) correctly: " + string.Join(", ", errors);
				return null;
			}
			return result;
		}

		/// <summary>
		/// Generate a list of random numbers between 1 ~ number of sides per dice per pair
		/// </summary>
		/// <param name="dice">A set of dice pair in (count, side) format.</param>
		/// <returns>List of results</returns>
		private static List<int> Roll(List<Tuple<int, int, int>> dice)
		{
			return dice.SelectMany(x => Roll(x.Item2, x.Item1, x.Item3)).ToList();
		}

		/// <summary>
		/// Generate a random number between 1 ~ number of sides per count, inclusively
		/// </summary>
		/// <remarks>Using GUID based randomizer, not as efficient but more "random"</remarks>
		/// <param name="side">Number of side per die</param>
		/// <param name="count">How many dice to roll</param>
		/// <param name="cheat">Cheating number</param>
		/// <returns>Dice roll outcome</returns>
		private static List<int> Roll(int side, int count = 1, int cheat = -1)
		{
			// Check for cheat
			return cheat > 0 ? Enumerable.Repeat(cheat, count).ToList() :
				Enumerable.Range(1, count).Select(x =>
					(int)Math.Ceiling(new Random(BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0)).NextDouble() * side)
			).ToList();
		}

		/// <summary>
		/// Get client IP address
		/// </summary>
		/// <returns>Client IP address values</returns>
		private static string GetIP()
		{
			var ip = (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null
				&& HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != "")
				? HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"]
				: HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
			if (ip.Contains(","))
				ip = ip.Split(',').First().Trim();
			return ip;
		}
		#endregion
	}
}