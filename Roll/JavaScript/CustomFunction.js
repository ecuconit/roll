﻿
// Configuration
var animationDuration = 500;

// Attach function to JQuery control for URL fetch
$.urlParam = function (name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	if (results === null) {
		return null;
	}
    else {
        return decodeURI(results[1]) || 0;
	}
};

// Scroll current view to designated control
$.scrollTo = function (to, duration, offset) {
    // If not specified, use default duration
    if (duration === null)
        duration = animationDuration;
    if (offset === null)
        offset = 0;
    $('html, body').animate({ scrollTop: $(to).offset().top + offset }, duration);
};

// Check to see if given string is null or empty
$.isNullOrEmpty = function (string) {
	// Check for simple case first
	return (string === undefined) || (string === null) ? true :
		typeof string === 'string' ? !string.trim() : false;
};

// Check to see if the provided value is of email string
$.isEmail = function (email) {
	// var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return regex.test(email);
};

// Set the primary submit key
$.SetSubmitKey = function (control) {
    console.log('setting keys: ');
    console.log(control);

    $.BufferSubmitKey(control);
    $.NegateSubmitKey(control);
};

// Negate submit key action on focusing
$.NegateSubmitKey = function (control) {
    control.off('focusin').focusin(function () { $.BufferSubmitKey(); });
    control.off('focusout').focusout(function () { $.RestoreSubmitKey(); });
};

// Buffer submit key control to stack
$.BufferSubmitKey = function (control) {
    //console.log(' ');
    //console.log('Pushing key: ');
    //console.log(control);

    tempSubmitKey.push(submitKey);
    submitKey = ((control === undefined) ? null : control);

    //console.log('Buffered Key: ');
    //console.log(tempSubmitKey);
    //console.log('Current Key: ');
    //console.log(submitKey);
    //console.log(' ');
};

// Restore last submit key control from temp
$.RestoreSubmitKey = function () {
    //console.log("PRINTING CALL STACK");
    //console.log(new Error().stack);

    var tempKey = tempSubmitKey.pop();
    submitKey = tempKey;

    //console.log(' ');
    //console.log('Popping key: ');
    //console.log(tempKey);
    //console.log('Buffered Key: ');
    //console.log(tempSubmitKey);
    //console.log('Current Key: ');
    //console.log(submitKey);
    //console.log(' ');
};

// Clear all current bootbox popups
$.ClearPopUp = function () {
    bootbox.hideAll();
};

// Custom alert function
$.Alert = function (title, message, okayCallback = null) {
    // Buffer key
    $.BufferSubmitKey();

    // Use bootbox alert
    bootbox.alert(message, function () {
        // Return submit key
        $.RestoreSubmitKey();

        // Calling okay message callback
        if (okayCallback)
            okayCallback();
    });
};

// Custom confirmation function
$.Confirmation = function (message, okayCallback, cancelCallback = null) {
    // Buffer key
    $.BufferSubmitKey();

    // Use bootbox confirmation
    bootbox.confirm(message, function (result) {
        // Return submit key
        $.RestoreSubmitKey();

        // Check for closing result
        if (!result) {
            // Calling cancel message callback 
            if (cancelCallback)
                cancelCallback();
            return;
        }

        // Calling okay message callback
        if (okayCallback)
            okayCallback();
    });
};

// Custom prompt function
$.Prompt = function (message, okayCallback = null, cancelCallback = null) {
    // Buffer key
    $.BufferSubmitKey();

    // Use bootbox confirmation
    bootbox.prompt(message, function (result) {
        // Return submit key
        $.RestoreSubmitKey();

        if ((result === undefined) || (result === null)) {
            if (cancelCallback)
                cancelCallback();
        }
        else {
            if (okayCallback)
                okayCallback(result);
        }
    });
};
$.PromptSelect = function (title, options, okayCallback, cancelCallback) { $.PromptWithOptions(title, options, 'select', okayCallback, cancelCallback); };
$.PromptCheckbox = function (title, options, okayCallback, cancelCallback) { $.PromptWithOptions(title, options, 'checkbox', okayCallback, cancelCallback); };
$.PromptWithOptions = function (title, options, type, okayCallback = null, cancelCallback = null) {
    // Buffer key
    $.BufferSubmitKey();

    // Initialize option
    if ((options === null) || (options.length <= 0))
        options = [{
            text: '[Invalid Option]',
            value: '-1'
        }];

    // Use bootbox selection/checkbox
    bootbox.prompt({
        title: title,
        inputType: type,
        inputOptions: options,
        callback: function (result) {
            // Return submit key
            $.RestoreSubmitKey();

            if ((result === undefined) || ($.isNullOrEmpty(result))) {
                if (cancelCallback)
                    cancelCallback();
            }
            else {
                if (okayCallback)
                    okayCallback(result);
            }
        }
    });
};

$.PromptDate = function (title, okayCallback, cancelCallback) { $.PromptWithType(title, 'date', okayCallback, cancelCallback); };
$.PromptEmail = function (title, okayCallback, cancelCallback) { $.PromptWithType(title, 'email', okayCallback, cancelCallback); };
$.PromptNumber = function (title, okayCallback, cancelCallback) { $.PromptWithType(title, 'number', okayCallback, cancelCallback); };
$.PromptPassword = function (title, okayCallback, cancelCallback) { $.PromptWithType(title, 'password', okayCallback, cancelCallback); };
$.PromptTextarea = function (title, okayCallback, cancelCallback) { $.PromptWithType(title, 'textarea', okayCallback, cancelCallback); };
$.PromptTime = function (title, okayCallback, cancelCallback) { $.PromptWithType(title, 'time', okayCallback, cancelCallback); };
$.PromptWithType = function (title, type, okayCallback = null, cancelCallback = null) {
    // Buffer key
    $.BufferSubmitKey();

    bootbox.prompt({
        title: title,
        inputType: type,
        callback: function (result) {
            // Return submit key
            $.RestoreSubmitKey();

            if ((result === undefined) || ($.isNullOrEmpty(result))) {
                if (cancelCallback)
                    cancelCallback();
            }
            else {
                if (okayCallback)
                    okayCallback(result);
            }
        }
    });
};

// Convert a size value into size string
$.ToSizeString = function (size) {
    if (size < 1000)
        // Bytes
        return size + 'B';
    else if (size < 1000000)
        return (size / 1000).toFixed(2) + 'KB';
    else if (size < 1000000000)
        return (size / 1000000).toFixed(2) + 'MB';
    else
        return (size / 1000000000).toFixed(2) + 'GB';
};

// Export 
$.ExportToExcel = function (e, table) {
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent($(table).html()));
    e.preventDefault();
};
// Export content of the search result box out to CSV file format
$.ExportToCSV = function ($table, filename) {
    var $headers = $table.find('tr:has(th)')
        , $rows = $table.find('tr:has(td)')

        // Temporary delimiter characters unlikely to be typed by keyboard
        // This is to avoid accidentally splitting the actual contents
        , tmpColDelim = String.fromCharCode(11) // vertical tab character
        , tmpRowDelim = String.fromCharCode(0) // null character

        // actual delimiter characters for CSV format
        , colDelim = '","'
        , rowDelim = '"\r\n"';

    // Grab text from table into CSV formatted string
    var csv = '"';
    csv += formatRows($headers.map(grabRow));
    csv += rowDelim;
    csv += formatRows($rows.map(grabRow)) + '"';

    // Data URI
    var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

    // For IE (tested 10+)
    if (window.navigator.msSaveOrOpenBlob) {
        var blob = new Blob([decodeURIComponent(encodeURI(csv))], {
            type: "text/csv;charset=utf-8;"
        });
        navigator.msSaveBlob(blob, filename);
    } else {
        /*
        // If this is attached to a link button
        $(this).attr({
            'download': filename
            , 'href': csvData
            // ,'target' : '_blank' //if you want it to open in a new window
        });
        */
        // Manual flush
        var downloadLink = document.createElement("a");
        downloadLink.href = csvData;
        downloadLink.download = filename;
        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
    }

    //------------------------------------------------------------
    // Helper Functions 
    //------------------------------------------------------------
    // Format the output so it has the appropriate delimiters
    function formatRows(rows) {
        return rows.get().join(tmpRowDelim)
            .split(tmpRowDelim).join(rowDelim)
            .split(tmpColDelim).join(colDelim);
    }
    // Grab and format a row from the table
    function grabRow(i, row) {

        var $row = $(row);
        //for some reason $cols = $row.find('td') || $row.find('th') won't work...
        var $cols = $row.find('td');
        if (!$cols.length) $cols = $row.find('th');

        return $cols.map(grabCol)
            .get().join(tmpColDelim);
    }
    // Grab and format a column from the table 
    function grabCol(j, col) {
        var $col = $(col),
            $text = $col.text();

        return $text.replace('"', '""'); // escape double quotes
    }
};

// Convert a base 64 string to BLOB format
$.Base64ToBlob = function (base64, mimetype, slicesize) {
    if (!window.atob || !window.Uint8Array) {
        // The current browser doesn't have the atob function. Cannot continue
        return null;
    }
    mimetype = mimetype || '';
    slicesize = slicesize || 512;
    var bytechars = atob(base64);
    var bytearrays = [];
    for (var offset = 0; offset < bytechars.length; offset += slicesize) {
        var slice = bytechars.slice(offset, offset + slicesize);
        var bytenums = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            bytenums[i] = slice.charCodeAt(i);
        }
        var bytearray = new Uint8Array(bytenums);
        bytearrays[bytearrays.length] = bytearray;
    }
    return new Blob(bytearrays, { type: mimetype });
};
// Trigger a download of a file with a base64 encoded string, file content mime type, and a default file name
$.DownloadFile = function (base64, mimetype, fileName)
{
    // Simple verification
    if (($.isNullOrEmpty(base64)) || ($.isNullOrEmpty(mimetype)))
        return 'Invalid file download parameter.';
    fileName = fileName || moment().format();

    // Convert file to BLOB format
    var blob = $.Base64ToBlob(base64, mimetype);
    // Do it the HTML5 compliant way
    if (window.navigator.msSaveOrOpenBlob) {
        // For IE (tested 10+)
        navigator.msSaveBlob(blob, fileName);
    }
    else if (window.URL && window.Blob && window.atob) {
        // Do for Chrome and Opera
        var a = document.createElement('a');
        document.body.appendChild(a);
        a.style = 'display: none';
        window.URL = window.URL || window.webkitURL;
        var url = window.URL.createObjectURL(blob);
        a.href = url;
        a.download = fileName;
        a.click();
        setTimeout(function () {
            // Causes a delay before removal
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);
        }, 420);
    }
    else {
        // Problem
        return 'Unable to determine browser file download capability, please use Chrome.';
    }
    return true;
}

// Clamp a number
$.Clamp = function (num, min, max) {
    return num <= min ? min : num >= max ? max : num;
};

// Round a number
$.Round = function (num, dp) {
    var numToFixedDp = Number(num).toFixed(dp);
    return Number(numToFixedDp);
}

// Adding leading zeroes to an 8 digits number
$.LeadingZero = function (format, string) {
	return (format + string).slice(-format.length);
}

// Stringify a .Net item to Java JSON string
$.Stringify = function (item) {
    return JSON.stringify(item).replace(/"/g, '\'');
};
// Return a stringified item back to .Net item
$.JSONParse = function (itemString) {
    return JSON.parse(itemString.replace(/\'/g, '"'));
};
// Turn a string into HTML safe string
$.ToHTMLString = function (string) {
    return string.replace(/&/g, '&amp;').replace(/>/g, '&gt;').replace(/</g, '&lt;').replace(/"/g, '&quot;').replace(/'/g, '&apos;').replace(/\n/g, '<br />');
};
// Check to see if the input string is type of back-end datetime
$.isDate = function (string) {
	return typeof(string) === 'string' ? string.startsWith('/Date') : false;
}; 
// Convert a database date object to a moment
$.DateParse = function (string, format) {
	var mom = moment(new Date(parseInt(string.substr(6))));
	return format === undefined ? mom : mom.format(format);
};

// Code modified from: http://jsfiddle.net/qxLn3h86/48/
$.TablesToExcel = (function () {
    var uri = 'data:application/vnd.ms-excel;base64,'
        , tmplWorkbookXML = '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?><Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">'
            + '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Author>Axel Richter</Author><Created>{created}</Created></DocumentProperties>'
            + '<Styles>'
            + '<Style ss:ID="Currency"><NumberFormat ss:Format="Currency"></NumberFormat></Style>'
            + '<Style ss:ID="Date"><NumberFormat ss:Format="Medium Date"></NumberFormat></Style>'
            + '</Styles>'
            + '{worksheets}</Workbook>'
        , tmplWorksheetXML = '<Worksheet ss:Name="{nameWS}"><Table>{rows}</Table></Worksheet>'
        , tmplCellXML = '<Cell{attributeStyleID}{attributeFormula}><Data ss:Type="{nameType}">{data}</Data></Cell>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
    return function (tables, wsnames, wbname, appname, callback) {
        var ctx = "";
        var workbookXML = "";
        var worksheetsXML = "";
        var rowsXML = "";

        for (var i = 0; i < tables.length; i++) {
            if (!tables[i].nodeType) tables[i] = document.getElementById(tables[i]);
            for (var j = 0; j < tables[i].rows.length; j++) {
                rowsXML += '<Row>'
                for (var k = 0; k < tables[i].rows[j].cells.length; k++) {
                    // ^o^ set data type, style, value, and formula in each cell
                    var dataType = tables[i].rows[j].cells[k].getAttribute("data-type");
                    var dataStyle = tables[i].rows[j].cells[k].getAttribute("data-style");
                    var dataValue = tables[i].rows[j].cells[k].getAttribute("data-value");
                    dataValue = (dataValue) ? dataValue : tables[i].rows[j].cells[k].innerHTML;
                    var dataFormula = tables[i].rows[j].cells[k].getAttribute("data-formula");
                    dataFormula = (dataFormula) ? dataFormula : (appname == 'Calc' && dataType == 'DateTime') ? dataValue : null;
                    ctx = {
                        attributeStyleID: (dataStyle == 'Currency' || dataStyle == 'Date') ? ' ss:StyleID="' + dataStyle + '"' : ''
                        , nameType: (dataType == 'Number' || dataType == 'DateTime' || dataType == 'Boolean' || dataType == 'Error') ? dataType : 'String'
                        , data: (dataFormula) ? '' : dataValue
                        , attributeFormula: (dataFormula) ? ' ss:Formula="' + dataFormula + '"' : ''
                    };
                    rowsXML += format(tmplCellXML, ctx);
                }
                rowsXML += '</Row>'
            }
            ctx = { rows: rowsXML, nameWS: wsnames[i] || 'Sheet' + i };
            worksheetsXML += format(tmplWorksheetXML, ctx);
            rowsXML = "";
        }

        ctx = { created: (new Date()).getTime(), worksheets: worksheetsXML };
        workbookXML = format(tmplWorkbookXML, ctx);

        console.log(workbookXML);

        var link = document.createElement("A");
        link.href = uri + base64(workbookXML);
        link.download = wbname || 'Workbook.xlsx';
        link.target = '_blank';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
        callback();
    }
})();

// Parse a CSV string to a list of lists of text
// Ref: https://stackoverflow.com/questions/8493195/how-can-i-parse-a-csv-string-with-javascript-which-contains-comma-in-data
$.CSVToTable = function (text) {
    let p = '', row = [''], ret = [row], i = 0, r = 0, s = !0, l;
    for (l of text) {
        if ('"' === l) {
            if (s && l === p) row[i] += l;
            s = !s;
        } else if (',' === l && s) l = row[++i] = '';
        else if ('\n' === l && s) {
            if ('\r' === p) row[i] = row[i].slice(0, -1);
            row = ret[++r] = [l = '']; i = 0;
        } else row[i] += l;
        p = l;
    }
    return ret;
}

// Randomizer
$.RandomNumber = function (side) {
	return Math.ceil(Math.random() * side);
};

$.RandomNumbers = function (count, side) {
	return Array.from({ length: count }, (v, i) => side).map(function (y) {
		return $.RandomNumber(y);
	});
};

// Statistical analysis
$.Statistic = function (numbers, simple) {
	// Calculate
	var sorted = numbers.sort(), hash = {};
	var max = sorted[sorted.length - 1], min = sorted[0], median = sorted.length % 2 === 1 ? sorted[Math.ceil(sorted.length / 2) - 1] : (sorted[(sorted.length / 2) - 1] + sorted[sorted.length / 2]) / 2,
		total = 0, average = 0, mode = [], standardDeviation;
	sorted.forEach(function (x) {
		// Count mode
		if (hash[x] === undefined) {
			hash[x] = 0;
			mode.push(x);
		}
		hash[x]++;

		// Sum
		total += x;
	});
	// Check for simplified analysis
	average = total / numbers.length;
	if ((simple !== null) && (simple === true)) {
		return { Total: total, Count: numbers.length, Mean: $.Round(average, 2) };
	}

	// Mode parse
	var maxCount = -1;
	mode.forEach(function (x) {
		if (hash[x] > maxCount)
			maxCount = hash[x];
	});

	// Finish
	return {
		Total: total, Count: numbers.length, Max: max, Min: min, Mean: $.Round(average, 2), Average: $.Round(average, 2), Median: median, Range: max - min,
		Mode: mode.filter(function (x) { return hash[x] === maxCount; }),
		StdDev: $.Round(Math.sqrt(numbers.map(function (x) { return Math.pow(x - average, 2); }).reduce(function (res, x) { return res + x; }, 0) / numbers.length), 2)
	};
};