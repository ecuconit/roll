﻿// Simple read content from an adapter's method
$.SimpleRead = function (adapter, method, params, success, failure, async) {
	// Verification
	if ((adapter == null) || (adapter == '')) {
		if (failure != null)
			failure('Simple read call need to reference an adapter.');
		else
			Debug('Simple read call need to reference an adapter.', true);
		return;
	}
	if ((method == null) || (method == '')) {
		if (failure != null)
			failure('Simple read call need to reference a method.');
		else
			Debug('Simple read call need to reference a method.', true);
		return;
	}
	// Parse async status
	if ((async == null) || (async != false))
		async = true;
	else
		async = false;

	// Calling AJAX adapter to read data
	var crossDomain = adapter.startsWith('http');
	$.ajax({
		type: 'POST',
		url: (crossDomain ? adapter : ('Adapter/' + adapter)) + '.aspx/' + method,
		contentType: 'application/json; charset=utf-8',
		data: (params == null ? {} : JSON.stringify(params)),
		dataType: 'json',
		async: async,
		cache: false,
		success: function (data) {
			// Verify AJAX result
			if ((data == null) || (data.d == null)) {
				// Show error
				if (failure != null)
					failure('No return data from AJAX initialization call.');
				else
					Debug('No return data from AJAX initialization call.', true);
				return;
			}

			// Return data as it is
			if (success != null)
				success(data.d);
		},
		failure: function (result) {
			console.log('fail');
			// Failure occurred
			if (failure != null)
				failure('Failure occurred.  Failed to perform simple read.  ' + result.d);
			else
				Debug('Failure occurred.  Failed to perform simple read.  ' + result.d, true);
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log('error');
			// Error occurred
			if (failure != null)
				failure('Error [' + xhr.status + '] on simple read: ' + xhr.responseText);
			else
				Debug('Error [' + xhr.status + '] on simple read: ' + xhr.responseText, true);
		}
	});
}

// Read content from an adapter's method
$.Read = function (adapter, method, params, success, failure, async) {
	// Verification
	if ((adapter == null) || (adapter == '')) {
		if (failure != null)
			failure('Simple read call need to reference an adapter.');
		else
			Debug('Simple read call need to reference an adapter.', true);
		return;
	}
	if ((method == null) || (method == '')) {
		if (failure != null)
			failure('Simple read call need to reference a method.');
		else
			Debug('Simple read call need to reference a method.', true);
		return;
	}
	// Parse async status
	if ((async == null) || (async != false))
		async = true;
	else
		async = false;

	// Calling AJAX adapter to read data
	var crossDomain = adapter.startsWith('http');
	$.ajax({
		type: 'POST',
		url: (crossDomain ? adapter : ('Adapter/' + adapter)) + '.aspx/' + method,
		contentType: 'application/json; charset=utf-8',
		data: (params == null ? {} : JSON.stringify(params)),
		dataType: 'json',
		async: async,
		cache: false,
		success: function (data) {
			// Verify AJAX result
			if ((data == null) || (data.d == null)) {
				// Show error
				if (failure != null)
					failure('No return data from AJAX initialization call.');
				else
					Debug('No return data from AJAX initialization call.', true);
				return;
			}

			// Attempt to parse data
			var jsob = null;
			try {
				// Parse return values
				jsob = JSON.parse(data.d);
			}
			catch (e) {
				console.log(data);
				// Show error
				if ($.isNullOrEmpty(data.d))
					Debug('Fail to parse data: ' + e, true);
				else
					Debug(data.d, true);
				return null;
			}

			// Populate form
			success(jsob);
		},
		failure: function (result) {
			// Failure occurred
			if (failure != null)
				failure('Failure occurred.  Failed to perform AJAX read.  ' + result.d);
			else
				Debug('Failure occurred.  Failed to perform AJAX read.  ' + result.d, true);
		},
		error: function (xhr, ajaxOptions, thrownError) {
			// Error occurred
			if (failure != null)
				failure('Error [' + xhr.status + '] on AJAX read: ' + xhr.responseText);
			else
				Debug('Error [' + xhr.status + '] on AJAX read: ' + xhr.responseText, true);
		}
	});
}

// Write parameters to an adapter's method, returns an object ID if success, negative number 
$.Write = function (adapter, method, params, success, failure, async) {
	// Verification
	if ((adapter == null) || (adapter == '')) {
		if (failure != null)
			failure('Simple read call need to reference an adapter.');
		else
			Debug('Simple read call need to reference an adapter.', true);
		return;
	}
	if ((method == null) || (method == '')) {
		if (failure != null)
			failure('Simple read call need to reference a method.');
		else
			Debug('Simple read call need to reference a method.', true);
		return;
	}
	// Parse async status
	if ((async == null) || (async != false))
		async = true;
	else
		async = false;

	// Calling AJAX adapter to write data
	var crossDomain = adapter.startsWith('http');
	$.ajax({
		type: 'POST',
		url: (crossDomain ? adapter : ('Adapter/' + adapter)) + '.aspx/' + method,
		contentType: 'application/json; charset=utf-8',
		data: (params == null ? {} : JSON.stringify(params)),
		dataType: 'json',
		async: async,
		cache: false,
		success: function (data) {
			// Verify AJAX result 
			if ((data == null) || (data.d == null)) {
				// Show error
				if (failure != null)
					failure('No return data from AJAX initialization call.');
				else
					Debug('No return data from AJAX initialization call.', true);
				return;
			}

			// Verify okay message
			if (data.d == okayMessage) {
				success(1);
				return;
			}
			// else if (Number.isInteger(data.d)) {
			else if (!isNaN(data.d)) {
				success(data.d);
				return;
			}
			Debug('Data not recognized', true);
			failure(data.d);
		},
		failure: function (result) {
			// Failure occurred
			if (failure != null)
				failure('Failure occurred.  Failed to perform AJAX write.  ' + result.d);
			else
				Debug('Failure occurred.  Failed to perform AJAX write.  ' + result.d, true);
		},
		error: function (xhr, ajaxOptions, thrownError) {
			// Error occurred
			if (failure != null)
				failure('Error [' + xhr.status + '] on AJAX write: ' + xhr.responseText);
			else
				Debug('Error [' + xhr.status + '] on AJAX write: ' + xhr.responseText, true);
		}
	});
}

// Display a message onto GUI, placed outside of JQuery to make things easier for after loading call
function Debug(message, failure) {
	// Disappear
	var alert = $('#debugMessage');

	// If display region doesn't exist, use bootbox
	if ((alert == null) || (alert.length <= 0)) {
		$.Alert('Message', message);
		console.log(message);
		return;
	}

	// Display region exist, use it
	alert.fadeOut(250);
	alert.removeClass('alert-success alert-info alert-danger');
	alert.empty();

	// Toggle display			
	if (failure == null) {
		alert.addClass('alert-info');
		message = '<strong>Info:</strong> ' + message;
	}
	else if (failure) {
		alert.addClass('alert-danger');
		message = '<strong>Error:</strong> ' + message;
	}
	else {
		alert.addClass('alert-success');
		message = '<strong>Success:</strong> ' + message;
	}
	alert.append(message);
	// console.debug(message);
	console.log(message);

	// Appear
	alert.fadeIn(350);
};